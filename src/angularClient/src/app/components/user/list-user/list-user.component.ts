import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.scss']
})
export class ListUserComponent implements OnInit {

  public users: User[] = [];

  constructor(public userService: UserService) { }

  ngOnInit() {
    this.getUsers();
  }

  public async getUsers() {
    this.users = await this.userService.getUsers();
    if (this.users != null) {
      console.log(this.users);
    } else {
      console.log("No existen usuarios");
    }
  }

  public async deleteUser(id: number){
    await this.userService.deleteUser(id);
    this.getUsers();
  }

}
