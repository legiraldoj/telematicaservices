import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { FormControl } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public users: User[] = [];

  public nameControl = new FormControl('');
  public ageControl = new FormControl();
  public addressControl = new FormControl('');
  public cityControl = new FormControl('');
  public emailControl = new FormControl('');
  public documentControl = new FormControl();

  constructor(public userService: UserService) { }

  ngOnInit() {
    this.getUsers();
  }

  public async getUsers() {
    this.users = await this.userService.getUsers();
    if (this.users != null) {
      console.log(this.users);
    } else {
      console.log("No existen usuarios");
    }
  }

  public async postUser(){
    await this.userService.addUser({
      name: this.nameControl.value,
      age: this.ageControl.value,
      address: this.addressControl.value,
      city: this.cityControl.value,
      email: this.emailControl.value,
      document: this.documentControl.value
    });
    this.nameControl.setValue(""),
    this.ageControl.setValue(""),
    this.addressControl.setValue(""),
    this.cityControl.setValue(""),
    this.emailControl.setValue(""),
    this.documentControl.setValue("")
    this.getUsers();
  }
  

}
