import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product';
import { ProductService } from 'src/app/services/product.service';
import { FormsModule, FormControl, FormArray } from '@angular/forms';

@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.scss']
})
export class ListProductComponent implements OnInit {

  public products: Product[] = [];
  public auxProducts: auxProduct[] = [];
  public auxComment: comment[] = [];
  public commentControl = new FormControl('');
  public comment: string[] = [];

  public isCheckControl = new FormControl(false);

  constructor(public productService: ProductService) { }

  ngOnInit() {
    this.getProducts();
  }
  public async getProducts() {
    this.products = await this.productService.getProduct();
    if (this.products == null) {
      console.log("No hay productos");
    } else {
      console.log(this.products);
      console.log();
    }
  }
  public async addListToComment(id: string) {
    await this.auxProducts.push({
      _id: id
    });
    console.log(this.auxProducts);
  }

  public async addComment() {
    await this.auxComment.push({
      comment: this.commentControl.value,
      products: this.auxProducts
    });
  }
}
interface auxProduct {
  _id: string;
}
interface comment {
  comment: string;
  products: auxProduct[];
}
