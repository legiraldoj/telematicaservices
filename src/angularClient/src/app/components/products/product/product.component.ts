import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product';
import { ProductService } from 'src/app/services/product.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  public products: Product[] = [];

  //controls
  public nameControl = new FormControl('');
  public infoControl = new FormControl('');
  public imageControl = new FormControl('');
  public commentControl = new FormControl('');

  constructor(public productService: ProductService) { }

  ngOnInit() {
    this.getProducts();
  }

  public async getProducts() {
    this.products = await this.productService.getProduct();
    if (this.products == null) {
      console.log("No hay productos");
    } else {
      console.log(this.products);
    }
  }

  public async postProduct() {
    await this.productService.addProduct({
      name: this.nameControl.value,
      info: this.infoControl.value,
      image: this.imageControl.value
    });
    this.nameControl.setValue("");
    this.infoControl.setValue("");
    this.imageControl.setValue("");
    this.getProducts();
  }

  public async deleteProduct(id: number){
    this.productService.deleteProduct(id);
    this.getProducts();
  }
}
