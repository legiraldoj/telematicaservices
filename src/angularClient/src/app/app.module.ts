import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/user/login/login.component';
import { RegisterComponent } from './components/user/register/register.component';
import { ProductComponent } from './components/products/product/product.component';
import { ListProductComponent } from './components/products/list-product/list-product.component';

import { AppRoutingModule } from './app-routing.module';
import { ProductService } from './services/product.service';
import { UserService } from './services/user.service';
import { ListUserComponent } from './components/user/list-user/list-user.component';

import { AmplifyAngularModule, AmplifyService } from 'aws-amplify-angular';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ProductComponent,
    ListProductComponent,
    ListUserComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AmplifyAngularModule
  ],
  providers: [ProductService, UserService, AmplifyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
