import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/user/login/login.component';
import { RegisterComponent } from './components/user/register/register.component';
import { ProductComponent } from './components/products/product/product.component';
import { ListProductComponent } from './components/products/list-product/list-product.component';
import { ListUserComponent } from './components/user/list-user/list-user.component';

const routes: Routes = [
  { path: '', redirectTo: "/index", pathMatch: 'full' },
  { path: 'productList', component: ListProductComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'userList', component: ListUserComponent },
  { path: 'product', component: ProductComponent },
  { path: 'index', component: LoginComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
