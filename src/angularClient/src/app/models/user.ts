export interface User {
    _id?: string;
    name?: string;
    age?: number;
    address?: string;
    city?: string;
    email?: string;
    document: number;
}