export interface Product{
    _id?: string;
    name?: string;
    info?: string;
    image?: string;
    comment?: string;
}