import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  public readonly rootURL = "http://localhost:3000/api/";

  constructor(public http: HttpClient) { }

  public async getProduct(): Promise<Product[]> {
    return new Promise<Product[]>((resolve, reject) => {
      this.http.get<Product[]>(this.rootURL + 'product').subscribe(resolve, reject);
    });
  }

  public async addProduct(task: Product) {
    return new Promise<Product[]>((resolve, reject) => {
      this.http.post<Product[]>(this.rootURL + 'product/', task).subscribe(resolve, reject);
    });
  }

  public async deleteProduct(id: number) {
    return new Promise<Product[]>((resolve, reject) => {
      this.http.delete<Product[]>(this.rootURL + 'product/' + id).subscribe(resolve, reject);
    });
  }
}
