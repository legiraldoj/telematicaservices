import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public readonly rootURL = "http://localhost:3000/api/";

  constructor(public http: HttpClient) { }

  public async getUsers(): Promise<User[]> {
    return new Promise<User[]>((resolve, reject) => {
      this.http.get<User[]>(this.rootURL + 'user').subscribe(resolve, reject);
    });
  }

  public async addUser(user: User) {
    return new Promise<User[]>((resolve, reject) => {
      this.http.post<User[]>(this.rootURL + 'user/', user).subscribe(resolve, reject);
    });
  }

  public async deleteUser(id: number) {
    return new Promise<User[]>((resolve, reject) => {
      this.http.delete<User[]>(this.rootURL + 'user/' + id).subscribe(resolve, reject);
    });
  }
}
