const cors = require('cors');//requiero lo que venga de cors
const express = require('express');
const app = express();
const path = require('path');

//const taskRoutes = require('./routes/task');
const productRoutes = require('./routes/product');
const userRouters = require('./routes/user');

//settings
app.set('views', path.join(__dirname, 'views'))
app.set('port', process.env.PORT || 3000);
app.engine('html', require('ejs').renderFile);//para renderizar html
app.set('view engine', 'ejs');

//middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));//recibir datos a partir de la url
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
  });

//routes
//app.use(indexRoutes);
//app.use('/api',taskRoutes);
app.use('/api', productRoutes);
app.use('/api', userRouters);

//static files
app.use(express.static(path.join(__dirname, 'dist/AngularClient')));//toma el código de los clientes

app.listen(app.get('port'), ()=>{
    console.log('server on port', app.get('port'));
})