const router = require('express').Router();
const mongojs = require('mongojs');
const db = mongojs('mean-db', ['products']);//url de la base de datos cualquiera aws

router.get('/product', (req, res, next) => {
    db.products.find((err, products) => {
        if (err) return next(err);
        res.json(products);
    });
});

router.get('/product/:id', (req, res, next) => {
    db.products.findOne({ _id: req.params.id}, (err, product) => {
        if (err) return next(err);
        res.json(product);
    });
});

router.post('/product', (req, res, next) => {
    const product = req.body;

    db.products.save(product, (err, product) => {
        if (err) return next(err);
        res.json(product);
    });
});

router.delete('/product/:id', (req, res, next) => {
    db.products.remove({ _id: mongojs.ObjectId(req.params.id) }, (err, result) => {
        if (err) return next(err);
        res.json(result);
    });
});

module.exports = router;