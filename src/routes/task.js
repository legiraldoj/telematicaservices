const router = require('express').Router();
const mongojs = require('mongojs');
const db = mongojs('mean-db', ['tasks']);//url de la base de datos cualquiera aws

router.get('/task', (req, res, next)=> {
    db.tasks.find((err, tasks)=>{
        if(err) return next(err);
        res.json(tasks);
    });
});

router.get('/task/:id', (req, res, next)=> {
    db.tasks.findOne({_id: req.params.id}, (err, task)=>{
        if(err) return next(err);
        res.json(task);
    });
});

router.post('/task', (req, res, next)=> {
    consttask =  req.body;
    if(!task.title || !(task.isDone + '')){
        res.status(400).json({
            error: 'Bad data'
        });
    }else{
        db.tasks.save(task, (err, task) =>{
            if(err) return next(err);
            res.json(task);
        });
    }
});

router.delete('/task/:id', (req, res, next) => {
    db.tasks.remove({_id: mongojs.ObjectId(req.params.id)}, (err, result) => {
        if(err) return next(err);
            res.json(result);
    });   
});

router.put('/task/:id', (req, res, next) => {
    const task = req.body;
    const updateTask = {};
    if(task.isDone){
        updateTask.isDone = task.isDone;
    }
    if(task.title){
        updateTask.title = task.isDone;
    }
    if(!updateTask){
        res.status(400).json({
            error: 'Bad request'
        });
    }
    db.task.update({_id: mongojs.ObjectId(req.params.id)}, (err, task) => {
        if(err) return next(err);
        res.json(task);
    });
});

module.exports = router;