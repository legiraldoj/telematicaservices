const router = require('express').Router();
const mongojs = require('mongojs');
const db = mongojs('mean-db', ['users']);//url de la base de datos cualquiera aws

router.get('/user', (req, res, next) => {
    db.users.find((err, users) => {
        if (err) return next(err);
        res.json(users);
    });
});

router.post('/user', (req, res, next) => {
    const user = req.body;

    db.users.save(user, (err, user) => {
        if (err) return next(err);
        res.json(user);
    });
});

router.delete('/user/:id', (req, res, next) => {
    db.users.remove({ _id: mongojs.ObjectId(req.params.id) }, (err, result) => {
        if (err) return next(err);
        res.json(result);
    });
});

module.exports = router;